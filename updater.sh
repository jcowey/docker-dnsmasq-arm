#!/bin/sh

addcatcherip='10.13.37.53'
configfile=/etc/dnsmasq.d/adblock.conf
listurlargs="hostformat=nohtml&showintro=0&mimetype=plaintext"
listurl="http://pgl.yoyo.org/adservers/serverlist.php?${listurlargs}"
extrasfile='/etc/adserver.manual'
reloadcmd='sh /etc/init.d/dnsmasq restart'
tmpfile="/tmp/.adlist.$$"
tmpconffile="/tmp/.dnsmasq.conf.$$"
fetchcmd="/usr/bin/wget -q -O $tmpfile $listurl"

$fetchcmd 

 [ -f "$extrasfile" ]  && cat $extrasfile >> $tmpfile
if  [ ! -s $tmpfile ] 
then
echo "temp file '$tmpfile' either doesn't exist or is empty; quitting"
exit
fi

cat $configfile | grep -v "address=" > $tmpconffile

while read line; do
    ADDRESS="/${line}/${addcatcherip}"
    echo "address=\"${ADDRESS}\"" >> $tmpconffile
done < $tmpfile 

mv $tmpconffile $configfile
$reloadcmd
rm $tmpfile
exit
