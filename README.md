<img src="https://drone.exnet.systems/api/badges/jack/docker-dnsmasq-arm/status.svg"/>
drone
# docker-dnsmasq-arm

Alpine ARM Docker image for [dnsmasq](http://www.thekelleys.org.uk/dnsmasq/doc.html).

## Usage

dnsmasq requires `NET_ADMIN` capabilities.

`docker run -p 53:53/tcp -p 53:53/udp -p 8080:8080 --cap-add=NET_ADMIN jcowey/dnsmasq-arm`

To see command-line arguments, run `docker run --rm schmich/dnsmasq:2.76-r1-arm --help`.
Additional configuration can be specified in `/etc/dnsmasq.conf`.

## Credits

Inspired by [Andy Shinn's dnsmasq image](https://github.com/andyshinn/docker-dnsmasq).
webproc

